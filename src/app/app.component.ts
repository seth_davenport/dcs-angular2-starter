import { Component, OnInit } from '@angular/core';


/**
 * The entry point into the angular app
 *
 * @export
 * @class AppComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'dcs-app',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  /**
   * Just some initial logging
   *
   * @memberOf AppComponent
   */
  ngOnInit(): void {
    console.timeEnd('bootstrap');
    console.log('App init successfull!!');
    // window['app'] = this;
  }

}
