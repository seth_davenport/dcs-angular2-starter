import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './app.routes';

import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';


@NgModule({
  declarations: [
    // components
    AppComponent,
    HomeComponent,
    NotFoundComponent,
  ],
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    }),
    CoreModule,
    SharedModule
  ]
})
export class AppModule { }
