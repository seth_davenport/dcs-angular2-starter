import { Component } from '@angular/core';


@Component({
  selector: 'dcs-home',
  templateUrl: './home.component.html',
  styles: [
    `
      h1 { color: darkblue; }
    `
  ]
})
export class HomeComponent {

  who: string = 'World';
}
